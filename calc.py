import numpy as np
import plotly as py
import plotly.graph_objs as go

equation = lambda x,y: 1 + (y*(2*x - 1))/(x*x)  
solved_equation = np.vectorize(lambda x: x**2) # exact solution of an IVP


def Euler_method(f, initial_point, x, h):
    y = np.zeros(len(x))
    y[0] = initial_point[1]
    for i in range(len(x) - 1):
        y[i+1] = (y[i] + h*f(x[i],y[i]))
    return y


def improved_Euler_method(f, initial_point, x, h):
    y = np.zeros(len(x))
    y[0] = initial_point[1]
    for i in range(len(x) - 1):
        t1 = f(x[i], y[i])
        t2 = f(x[i + 1], y[i] + h*t1)
        y[i+1] = y[i] + (h/2)*(t1 + t2)
    return y


def Runge_Kutta_method(f, initial_point,x, h):
    y = np.zeros(len(x))
    y[0] = initial_point[1]
    for i in range(len(x) - 1):
        k1 = h * f(x[i], y[i])
        k2 = h * f(x[i] + h/2, y[i] + k1/2)
        k3 = h * f(x[i] + h/2, y[i] + k2/2)
        k4 = h * f(x[i] + h, y[i] + k3)
        y[i+1] = y[i] + (1/6) * (k1 + 2*k2 + 2*k3 + k4)
    return y


def total_error(aproximation, initial_point, end_point, n, N):
    ns = list(range(n, N))
    y = []
    for n in ns:
        h = (end_point - initial_point[0])/n
        X = np.arange(initial_point[0], end_point+h, h)
        error = list(map(abs, solved_equation(X) - aproximation(equation, initial_point, X, h)))
        y.append(max(error))
    return y


def get_plots(initial_point, end_point, h, n, N):

    X = np.arange(initial_point[0], end_point + h, h)
    s = solved_equation(X)
    result = {}
    # Euler mthod
    Y = Euler_method(equation, initial_point, X, h)
    e = list(map(abs, s - Y))
    result['euler'] = py.offline.plot({"data": [go.Scatter(x=X, y=Y, name="Euler"), go.Scatter(x=X, y=s, name="Exact")],"layout": go.Layout(title="Euler")}, output_type="div")
    result['euler_error'] = py.offline.plot({"data": [go.Scatter(x=X, y=e, name="Error of Eulare method")], "layout": go.Layout(title="Error of Eulare method")}, output_type="div")
    # Improved euler method
    Y = improved_Euler_method(equation, initial_point, X, h)
    e = list(map(abs, s - Y))
    result['imp_euler'] = py.offline.plot({"data": [go.Scatter(x=X, y=Y, name="Improved Euler"), go.Scatter(x=X, y=s, name="Exact")],"layout": go.Layout(title="Improved Euler")}, output_type="div")
    result['imp_euler_error'] = py.offline.plot({"data": [go.Scatter(x=X, y=e, name="Error of Improved Eulare method")], "layout": go.Layout(title="Error of Improved Eulare method")}, output_type="div")
    # Runge-Kutta method
    Y = Runge_Kutta_method(equation, initial_point, X, h)
    e = list(map(abs, s - Y))
    result['runge_kutta'] = py.offline.plot({"data": [go.Scatter(x=X, y=Y, name="Runge-Kutta"), go.Scatter(x=X, y=s, name="Exact")],"layout": go.Layout(title="Runge-Kutta")}, output_type="div")
    result['runge_kutta_error'] = py.offline.plot({"data": [go.Scatter(x=X, y=e, name="Error of Runge-Kutta method")], "layout": go.Layout(title="Error of Runge-Kutta method")}, output_type="div")

    N_total_error = list(range(n, N+1))

    Y_total_error_euler = total_error(Euler_method, initial_point, end_point, n, N)
    
    Y_total_error_imp_euler = total_error(improved_Euler_method, initial_point, end_point, n, N)

    Y_total_error_runge_kutta = total_error(Runge_Kutta_method, initial_point, end_point, n, N)

    result['total_error'] = py.offline.plot({"data": [go.Scatter(x=N_total_error, y=Y_total_error_euler, name="Total error of Euler method"), \
                                                       go.Scatter(x=N_total_error, y=Y_total_error_imp_euler, name="Total error of Improved Euler method"), \
                                                       go.Scatter(x=N_total_error, y=Y_total_error_runge_kutta, name="Total error of Runge-Kutta method")], \
                                              "layout": go.Layout(title="Total error")}, output_type="div")
    return result