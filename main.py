from flask import Flask, render_template, request
from calc import get_plots as get_plots_


app = Flask(__name__)


@app.route("/")
def index():
    return render_template("index.html")


@app.route("/get_plots")
def get_plots():
    x = float(request.args.get('x_init'))
    y = float(request.args.get('y_init'))
    X = float(request.args.get('X'))
    h = float(request.args.get('h'))
    n = int(request.args.get('n'))
    N = int(request.args.get('N'))
    plots = get_plots_((x,y), X, h, n, N)
    return render_template('plots.html', **plots)
    

if __name__ == '__main__':
    app.run(host="0.0.0.0", port=5000)
