# DE Assignment 
## Installation

1. Install npm and python3
2. Create python envirnment
```
    python3 -m venv venv
```
3. Install packages
```
    source venv/bin/activate
    pip install -r requirements.txt
    npm install
```

## Running
```
    npm start
```